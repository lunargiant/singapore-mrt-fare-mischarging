# Discovery of Fare Errors in Singapore's Public Transport Rail (MRT) 

## Abstract

In analysing the public rail transit system, a number of errors were found in the fare system. The errors were cases of undercharging of fares for some routes. The basis of determining these fares as undercharging is their deviation from the distance-based fare structure adopted by Singapore's public transport network.

---

## Background

The public transport system is managed by the Land Transport Authority (LTA), a statutory board of the Ministry of Transport (MoT) of Singapore. The fare structure and its occasional revisions are overseen by the Public Transport Council (PTC) which is also a government appointed body.

![Singapore public rail transit system](images/Singapore_MRT_and_LRT_System_Map.svg "Singapore public rail transit system")
<p align="center">Figure 1: Singapore public rail transit system</p>

**Distance-based fare charging:** Public transport fares are charged on a distance-based system which came into effect on 3 July 2010 during then Transport Minister Raymond Lim's tenure. Under this system, the fare charged on trips in the public rail and bus network is essentially based on the distance travelled by a commuter as he rides and changes his mode(s) of transport. It differs from the previous system of fixed rebates given for transfers. A distance-based fare is based on the cumulative distance over a whole journey. This was and is quite a fair system even though it is not a constant linear cost/distance ratio over the whole distance range of the fare structure and there were previously some differences in fare per unit distance for different rail lines (which has since been harmonized).

### Scope of study
- The subject of the study and analysis behind this discovery of errors were Singapore's Mass Rapid Transit (MRT) and Light Rail Transit (LRT) public transit network. Features include its network topology and the distance-based fare system.
- The period covered mainly the later part of year 2021. The fare structure in place had taken effect on 28 December 2019 and is expected to be revised on 26 December 2021.
- Information and data used are all openly available from LTA and other public sources such as the PTC.
- Whereas effort was made to be as extensive as possible in coverage and correctness, there could be blindspots and mistakes.

### Definitions and terms

- **Train/Rail/MRT**: In this article, these terms in general refer to the MRT(mass rapid transit) and/or LRT(light rail transit) systems built and overseen by the government.
- **Station**: The term _station_ in this article refers to a physical station as opposed to the station that is part of a rail line. For example, Serangoon station has two lines traversing and stopping at it - Circle Line (CCL) and Northeast Line (NEL). Correspondingly, the _line stations_ in Serangoon _station_ for CCL and NEL are named CC13 and NE12 respectively. In this article, any reference to a _station_ would refer to the first definition above (physical station) unless specified otherwise.
- **Route**: The term _route_ in this article refers to a pair of origin-destination stations that a commuter might embark and disembark from. Explicitly, _route_ does not refer to the actual path (of stations) that is traversed by a commuter in between the origin-destination stations. Warning: This definition deviates from most conventional usage of "route"(which usually refers to the aforementioned latter definition).

---

## Errors Uncovered

**Route-fare symmetry**: The fare system is assumed to be route-symmetric, i.e. the same fare is levied for the same origin-destination route and in reverse. For brevity, this article will refer to only one route per unique station-station pair. For example, Bishan-Redhill and not both Bishan-Redhill and Redhill-Bishan. Commuter traffic is of course not likely to be symmetric.

Eight(8) routes were discovered to be undercharging for the _adult_ fare type when comparing their nominal LTA distances to the applicable PTC distance fare structure. It can be seen that the quantum of undercharging was usually $0.04 except for one route where it was $0.06. The same quantum of undercharging also occurred for the _workfare transport concession scheme (WTCS)_ fare type.

|Start|End|LTA Fare($)|Distance-based Fare($)|LTA Distance(m)|
|-----|---|----------:|-----------:|----------:|
|Aljunied|Expo|1.40|1.44|9200|
|Bishan|Redhill|1.56|1.60|12600|
|Bishan|Toa Payoh|0.88|0.92|2100|
|Bishan|Khatib|1.40|1.44|8700|
|Khatib|Redhill|1.89|1.93|21300|
|Khatib|Toa Payoh|1.48|1.52|10800|
|Orchard|Queenstown|1.38|1.44|8600|
|Redhill|Toa Payoh|1.48|1.52|10500|

<p align="center">Table 1: Route Fare Errors Discovered</p>

A screenshot of the actual fare levied for Bishan → Toa Payoh, $0.88, which clearly falls below the minimum fare for adults in the current fare structure.
\
![Bishan → Toa Payoh fare error](images/LTA_fare_MRT_Bishan-ToaPayoh.png "Bishan → Toa Payoh fare error")
<p align="center">Figure 2: Bishan → Toa Payoh fare error ($0.88) which is below the current minimum fare</p>

\
The routes listed are for one direction for each station(A)-station(B) pair but the fare discrepancies are also found to be the same in the other direction, i.e. A to B costs the same as B to A.

The errors found were both in the online (Web) fare check systems as well as in the actual physical fare charging system.

**Other Fare Types:** No discrepancies were found for other fare types - namely, _standard_, _student_, _persons with disabilities_, and _seniors_. Also, not covered in the study are other types of fares such as fixed price/period passes (e.g. tourist passes). Also not studied are early commuter discounts - where up to $0.50 is discounted from commuters entering stations before 7.45am on weekdays.

---

## Revenue Loss Due to Undercharging

To estimate the cost of the undercharging errors, that is the revenue loss, the number of trips for the _adult_ and _workfare transport concession scheme_ fare types need to be known and multiplied by the corresponding fare discrepancies.

However, the required information is unavailable and hence the exact losses could not be determined.

What is known is the total number of all trips for each route. An upper bound of the fare losses can thus be estimated. This worked out to be about $85k per month in the last few months.

> Estimated upper bound of revenue loss: **$85K per month**

To estimate the total loss of these errors for all the time that they were happening, it is assumed that the errors were present since the fares were last updated on 28 December 2019. Approximately 24 months have passed as of this article's date (December 2021) and a worst case nett loss of about $2M (24 x $85K) may thus have been incurred.

> Total estimated upper bound of revenue loss **over 2 years: $2M**

**IMPORTANT:** Note that the above are estimates of the likely worst case losses. They would be more accurate if the actual number of trips per fare type are known.

---

## Analysis of Errors

The number of stations operating for embarkation/disembarkation is currently 164. This gives a combination of 13,366 unique station-station pairs. The percentage of erroneous routes is thus:

> 8 ÷ 13366 = **0.06%** (routes with errors)

The number of routes that have errors are very few compared to the total number of routes. They are however occurring in some of the more central parts of the transport network (Toa Payoh, Bishan, Redhill, etc.).

A total of eight(8) stations were involved in the erroneous routes. It is noted that some stations (Bishan, Khatib, Redhill, Toa Payoh) occur more frequently within the set of erroneous routes.

    1. Aljunied - 1/8
    2. Bishan - 3/8
    3. Expo - 1/8
    4. Khatib - 3/8
    5. Orchard - 1/8
    6. Queenstown - 1/8
    7. Redhill - 3/8
    8. Toa Payoh - 3/8

**Causes of errors** may be localised to:

- network structure data
- fare structure data
- algorithmic errors (derivation of shortest paths or other computations)
- other errors such as in data handling or transfer

The above categories are not mutually exclusive - so multiple causes may exist.

**Shortest path principle**: A distance-fare system should logically be implemented based on the shortest path principle in a path independent charging transit network (as the Singapore MRT is). In graph(network) theory, the Djikstra algorithm is one such method to derive the shortest paths. Standard libraries and software applications exist to run these derivations. It is expected that public transport planners and their vendors would be using these or other verified software. With the correct data input, the derived distances and fares should have no mistakes. Unfortunately, the publicly available network distances (between stations) were found to be problematic - see _Data Issues_ below.

It could be hypothesized that some of the network distances near the stations and/or along the routes had errors. Even so, it is difficult to track down the actual source of the errors. As explained later, the lack of precise/correct network distances hampered further investigation. 

It is noted again that the same set of errors occurs in both the fare calculator application as well as the actual fares charged to commuters.  

---

## Data Issues

The lack, incorrectness or imprecision of publicly available data hampered attempts to estimate the fare losses and further analysis to isolate the cause of the fare errors.

* **Lack of Fare Type Trip Numbers:**
The lack of exact fare type (adult, standard, student, workface concession, senior) trip numbers prevent the derivation of more accurate estimates of the revenue losses arising from the fare errors. See _Revenue Loss Due to Undercharging_ above.

* **Imprecise or erroneous network distances:**
When trying to build the network graph, it was found that some distances do not add up. This hampered proper modelling of the transport network. Modelling the network is important to derive the shortest paths and distances that correspond to the fares to be charged.

    For example:

    The path from Toa Payoh to Newton is:

        Toa Payoh → Novena → Newton

    The distance for Toa Payoh → Novena is 1.5km (Figure 3.1). The distance for Novena → Newton is 1.2km (Figure 3.2). Therefore the distance for Toa Payoh → Newton should be 1.5km + 1.2km = **2.7km**. 

    Yet LTA's nominal distance for Toa Payoh → Newton is in fact **2.6km** (Figure 3.3) - a **shortage of 0.1km**.

    ![Toa Payoh → Novena](images/LTA_fare_MRT_ToaPayoh-Novena.png "Toa Payoh → Novena fare/distance")
    <p align="center">Figure 3.1: Toa Payoh → Novena distance = 1.5km</p>

    ![Novena → Newton](images/LTA_fare_MRT_Novena-Newton.png "Novena → Newton fare/distance")
    <p align="center">Figure 3.2: Novena → Newton distance = 1.2km</p>

    ![Toa Payoh → Newton](images/LTA_fare_MRT_ToaPayoh-Newton.png "Toa Payoh → Newton fare/distance")
    <p align="center">Figure 3.3: Toa Payoh → Newton distance = 2.6km</p>

    \
    The discrepancy may arise from rounding errors but could also be actual input errors. It would help if LTA checks and clarifies.

    In the case of rounding errors due to the lack of precision in the public facing data, it would help for LTA to publish precise(not rounded) distances to aid future and further studies of the transport network.

    **Note:** whereas the distance discrepancy case cited above did not lead to a fare error, it can contribute cumulatively to errors in other longer routes that passes through the same stations.

---

## Potential Undiscovered Errors

While there are many other potential errors in the system that could arise, the specific one to highlight is the bus network which is a substantial complement to the MRT network. This was not studied in this work. Notably, it is more complex and is not path independent when comes to fare charging.

---

## Commentary

### (or thoughts on why no one has reported/discovered the errors for 2 years)

It is interesting that undercharging on a few but significant major routes remained unreported for so long. It is quite inconceivable that not a single commuter on these routes, to date (for about 2 years), had become aware of their undercharged fares. However, in fairness, the issue of error discovery is made more difficult by the use of fare charging systems such as SimplyGo which does not display fares immediately on the buses' card tap terminals. Yet, it is perhaps human nature that those who are aware chose not to report this fortuitous albeit small self benefit.

Whereas the public might be expected to play a part in the discovery and reporting of errors, it is still primarily the responsibility of the government body in charge. Perhaps the Covid-19 pandemic has made things more difficult.

---

## Summary & Conclusion
- The sum of public revenue loss is at worst as much as $2M over the past 2 years. It may be relatively small but is still significant.
- The errors lead to unfairness in the fare system and could lead the government (including bodies such as the PTC) to make less accurate decisions on fare changes.
- The cause of these errors could not be definitively isolated in the study.
- The likelihood of fraud or illicit activities (such as by unauthorized computer systems access) cannot be logically ruled out, however unlikely.
- Lack, correctness or precision of data hampered further investigation of the issue.
- Long running unchecked errors with significant revenue loss on such an expansive and highly utilised public sytem points to room for improvement in post-implementation checks as well as the need to foster and facilitate independent analyses by interested public members, academics and analysts. This would be in line with data openness and data literacy initiatives by the government. 

It is hoped that the Singapore government, specifically the Ministry of Transport, releases more detailed data (as highlighted) to improve the study and analysis of the transport network which could result in early discovery of errors such as those revealed by this article.

If there are mistakes made above, the author apologises and looks forward to be corrected.


\
\
Author: Lunar Giant\
Dated: 20 December 2021

---

## Attribution & Acknowledgement

* Whereas this article has pointed out the lack/precision of some specific data, the Singapore government including the Ministry of Transport and Land Transport Authority has very extensive publishing of open data for public use. The author is most grateful for this.
* Data from various government sources including Land Transport Authority and Public Transport Council.
* Screenshots taken from LTA Fare Calculator website.
* Singapore MRT Map from Wikipedia: By <a href="//commons.wikimedia.org/wiki/User:Aforl" title="User:Aforl">Aforl</a>. - <span class="int-own-work" lang="en">Own work</span>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=31339406">Link</a>


## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="images/creative-commons-by-nd-4.0-88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">Creative Commons Attribution-NoDerivatives 4.0 International License</a>. The no-derivative option is chosen to discourage any substantial misrepresentation of this work. Otherwise, feel free to quote this work with attribution.


## About the Author

Lunar Giant (pseudonym) is an analyst with wide ranging interests. Reachable at twitter.com/lunargiant1.
